package main

import (
	"github.com/zeebe-io/zeebe/clients/go/pkg/zbc"
	"sync"
)

type ZeebeClient struct {
	client zbc.Client
}
var singleton *ZeebeClient
var once sync.Once

func GetZbClient() *ZeebeClient {
	once.Do(func() {
		singleton = &ZeebeClient{client: *applyConnectionToZeebe()}
	})
	return singleton
}

func (zb *ZeebeClient) GetClient() zbc.Client {
	return zb.client
}

func applyConnectionToZeebe() *zbc.Client {

	newClient, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         ZeebeBrokerAddr,
		UsePlaintextConnection: true,
	})

	if err != nil {
		panic(err)
	}

	return &newClient
}