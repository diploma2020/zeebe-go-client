package main

func main() {
	InitZeebe()
	go InitRabbit()
	StartHTTPServer()
}