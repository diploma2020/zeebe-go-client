package main

type CompleteJobDTO struct {
	WorkFlowInstanceKey int64 `json:"instance"`
	JobKey int64 `json:"job"`
	Variables map[string]interface{} `json:"variables"`
}

type DeployWorkflowDTO struct {
	FileName string `json:"file"`
}

type Message struct {
	Headers Header
	Body map[string]interface{}
}

type Header struct {
	XJobKey int64
	XWorkflowInstanceKey int64
}