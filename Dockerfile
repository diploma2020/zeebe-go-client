FROM golang:1.13

ENV GO111MODULE=on
ENV PORT=8000
WORKDIR /app/server
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .

RUN go build
CMD ["./zeebe-go-client"]