package main

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
)

func StartHTTPServer() {
	mux := http.NewServeMux()
	mux.HandleFunc("/healthcheck/", makeHandler(healthCheckHandler))
	mux.HandleFunc("/deploy/", makeHandler(deployWorkflowHandler))
	log.Fatal(http.ListenAndServe(":"+HTTPPort, mux))
}

var validPath = regexp.MustCompile("^/(healthcheck|deploy)/$")

func makeHandler(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r)
	}
}

func sendResponse(w http.ResponseWriter, result interface{})  {
	js, err := json.Marshal(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(js)
}

func healthCheckHandler(w http.ResponseWriter, r *http.Request) {
	body := make(map[string]interface{})
	body["ok"] = true

	sendResponse(w, "Hello World")
}

func deployWorkflowHandler(w http.ResponseWriter, r *http.Request)  {
	dto := DeployWorkflowDTO{}

	err := json.NewDecoder(r.Body).Decode(&dto)

	if err != nil {
		log.Println(err)
	}

	log.Println(dto.FileName)

	response := DeployWorkflow(dto)
	sendResponse(w, map[string]interface{} {
		"key": response.GetKey(),
		"workflows": response.GetWorkflows(),
	})
}