module zeebe-go-client

go 1.13

require (
	github.com/fatih/structs v1.1.0
	github.com/mitchellh/mapstructure v1.2.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/zeebe-io/zeebe/clients/go v0.0.0-20200416100212-a1ce8718fd01
)
