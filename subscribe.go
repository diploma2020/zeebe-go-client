package main

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
	"github.com/streadway/amqp"
	"log"
	"strings"
)

func Subscribe() {

	q, err := session.Channel.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = session.Channel.QueueBind(
		q.Name,        // queue name
		"zeebe.job.complete.#",             // routing key
		"zeebe.job", // exchange
		false,
		nil)
	failOnError(err, "Failed to bind a queue")

	msgs, err := session.Channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")
	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	forever := make(chan bool)

	listen(msgs)

	<-forever
}

func listen(msgs <-chan amqp.Delivery) {
	for d := range msgs {

		body := make(map[string]interface{})
		err := json.Unmarshal(d.Body, &body)
		failOnError(err, "Cannot unmarshal json body")

		var headers Header
		err = mapstructure.Decode(d.Headers, &headers)
		failOnError(err, "Wrong headers come")

		message := Message{Headers:headers, Body:body}

		redirect(splitByDotANdReturnLast(d.RoutingKey), message)
	}
}

func splitByDotANdReturnLast(s string) string {
	arr := strings.Split(s, ".")
	return arr[len(arr)-1]
}

func redirect(jobType string, message Message)  {
	switch jobType {
		case "create-workflow-instance":
			CreateWorkflowInstance(message.Body)
		case "initialization":
			log.Println(message)
			CompleteJob(message)
		case "review-app":
			log.Println(jobType)
			CompleteJob(message)
		case "meeting":
			log.Println(jobType)
			CompleteJob(message)
		case "prepare-offer":
			log.Println(jobType)
			CompleteJob(message)
		case "retry-offer-submit":
			log.Println(jobType)
			CompleteJob(message)
		case "offer-response":
			log.Println(jobType)
			OfferResponseTask(message)
		case "call-partner-offer":
			log.Println(jobType)
			CompleteJob(message)
		case "offer-call-response":
			log.Println(jobType)
			OfferCallResponseTask(message)
		case "create-acc":
			log.Println(jobType)
			CompleteJob(message)
		case "retry-account-submit":
			log.Println(jobType)
			CompleteJob(message)
		case "acc-response":
			log.Println(jobType)
			AccResponseTask(message)
		case "call-partner-acc":
			log.Println(jobType)
			CompleteJob(message)
		case "acc-call-response":
			log.Println(jobType)
			AccCallResponseTask(message)
		case "reject-app":
			log.Println(jobType)
			CompleteJob(message)
		case "message-send":
			log.Println(jobType)
			CompleteJob(message)
		default:
			log.Printf("no such subscriber: [x] %s", jobType)
	}
}

