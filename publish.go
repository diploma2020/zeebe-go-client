package main

import (
	"encoding/json"
	"github.com/fatih/structs"
	"github.com/streadway/amqp"
	"log"
)

func Publish(routingKey string, message Message)  {
	conn, err := amqp.Dial(RabbitUrl)
	if err != nil {
		panic("cannot connect")
	}

	channel, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer channel.Close()

	body, err := json.Marshal(message.Body)
	headers := structs.Map(&message.Headers)

	failOnError(err, "Cannot marshal body to json")

	log.Println("the job ", routingKey, " started ", message)

	err = channel.Publish(
		"zeebe.job",         // exchange
		"zeebe.job.start." + routingKey, // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: 	"application/json",
			Headers: 		headers,
			Body:        	body,
		})

	failOnError(err, "Failed to publish a message")
}