package main

import (
	"context"
	"fmt"
	"github.com/zeebe-io/zeebe/clients/go/pkg/entities"
	"github.com/zeebe-io/zeebe/clients/go/pkg/pb"
	"github.com/zeebe-io/zeebe/clients/go/pkg/worker"
	"log"
	"strconv"
)

var zbClient *ZeebeClient

func InitZeebe() {
	zbClient = GetZbClient()

	go InitTask()
	go ReviewAppTask()
	go rejectAppTask()
	go meetingTask()
	go collectFeedbackTask()
	go prepareOfferTask()
	go submitOfferTask()
	go createAccountTask()
	go retryAccountSubmitTask()
	go callPartnerAccTask()

	go retryOfferSubmitTask()
	go callPartnerOfferTask()

	// Messaging
	go sendMessageTask()
}

func DeployWorkflow(dto DeployWorkflowDTO) *pb.DeployWorkflowResponse {
	c := context.TODO()

	topology, err := zbClient.GetClient().NewTopologyCommand().Send(c)
	if err != nil {
		panic(err)
	}

	for _, broker := range topology.Brokers {
		fmt.Println("Broker", broker.Host, ":", broker.Port)
		for _, partition := range broker.Partitions {
			fmt.Println("  Partition", partition.PartitionId, ":", roleToString(partition.Role))
		}
	}

	deployWorkflowResponse, err := zbClient.GetClient().NewDeployWorkflowCommand().AddResourceFile("./resources/" + dto.FileName + ".bpmn").Send(c)

	log.Println(deployWorkflowResponse)
	log.Println(err)
	if err != nil {
		panic(err)
	}

	return deployWorkflowResponse
}

func roleToString(role pb.Partition_PartitionBrokerRole) string {
	switch role {
	case pb.Partition_LEADER:
		return "Leader"
	case pb.Partition_FOLLOWER:
		return "Follower"
	default:
		return "Unknown"
	}
}

func CreateWorkflowInstance(variables map[string]interface{}) map[string]interface{}  {
	c := context.TODO()

	request, err := zbClient.GetClient().NewCreateInstanceCommand().BPMNProcessId("Process_onboarding_diagram").LatestVersion().VariablesFromMap(variables)
	if err != nil {
		panic(err)
	}

	msg, err := request.Send(c)
	if err != nil {
		panic(err)
	}

	fmt.Println(msg)

	return variables
}

func sendMessageTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("message-send").Handler(handleJob).Open()
}


func InitTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("initialization").Handler(handleJob).Open()
}

func ReviewAppTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("review-app").Handler(handleJob).Open()
}

func rejectAppTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("reject-app").Handler(handleJob).Open()
}

func meetingTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("meeting").Handler(handleJob).Open()
}

func collectFeedbackTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("collect-feedback").Handler(handleJob).Open()
}

func prepareOfferTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("prepare-offer").Handler(handleJob).Open()
}

func retryOfferSubmitTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("retry-offer-submit").Handler(handleJob).Open()
}

func callPartnerOfferTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("call-partner-offer").Handler(handleJob).Open()
}

func callPartnerAccTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("call-partner-acc").Handler(handleJob).Open()
}

func submitOfferTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("submit-offer").Handler(handleJob).Open()
}

func createAccountTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("create-acc").Handler(handleJob).Open()
}

func retryAccountSubmitTask()  {
	_ = zbClient.GetClient().NewJobWorker().JobType("retry-account-submit").Handler(handleJob).Open()
}

func OfferResponseTask(message Message)  {
	correlationId := message.Body["uuid"].(string)

	request, err := zbClient.GetClient().
		NewSetVariablesCommand().
		ElementInstanceKey(message.Headers.XWorkflowInstanceKey).
		VariablesFromMap(message.Body)

	if err != nil {
		log.Println(err)
	}

	request.Send(context.Background())

	_,_ = zbClient.GetClient().
		NewPublishMessageCommand().
		MessageName("offer-response").
		CorrelationKey(correlationId).
		Send(context.TODO())
}

func AccResponseTask(message Message)  {
	correlationId := message.Body["uuid"].(string)

	request, err := zbClient.GetClient().
		NewSetVariablesCommand().
		ElementInstanceKey(message.Headers.XWorkflowInstanceKey).
		VariablesFromMap(message.Body)
	if err != nil {
		log.Println(err)
	}

	request.Send(context.Background())


	_,_ = zbClient.GetClient().NewPublishMessageCommand().MessageName("acc-response").CorrelationKey(correlationId).Send(context.TODO())
}

func OfferCallResponseTask(message Message)  {
	correlationId := message.Body["uuid"].(string)

	request, err := zbClient.GetClient().
		NewSetVariablesCommand().
		ElementInstanceKey(message.Headers.XWorkflowInstanceKey).
		VariablesFromMap(message.Body)
	if err != nil {
		log.Println(err)
	}

	request.Send(context.Background())


	_,_ = zbClient.GetClient().NewPublishMessageCommand().MessageName("offer-call-response").CorrelationKey(correlationId).Send(context.Background())
}

func AccCallResponseTask(message Message)  {
	correlationId := message.Body["uuid"].(string)

	request, err := zbClient.GetClient().
		NewSetVariablesCommand().
		ElementInstanceKey(message.Headers.XWorkflowInstanceKey).
		VariablesFromMap(message.Body)
	if err != nil {
		log.Println(err)
	}

	request.Send(context.Background())


	_,_ = zbClient.GetClient().NewPublishMessageCommand().MessageName("acc-call-response").CorrelationKey(correlationId).Send(context.Background())
}

func retriesError(dto CompleteJobDTO)  {
	_, _ = zbClient.GetClient().
		NewThrowErrorCommand().
		JobKey(dto.JobKey).ErrorCode("1").
		ErrorMessage(strconv.FormatInt(dto.WorkFlowInstanceKey, 10) + " retries ended").
		Send(context.Background())
}

func handleJob(client worker.JobClient, job entities.Job) {
	jobKey := job.GetKey()
	workflowInstanceKey := job.GetWorkflowInstanceKey()

	variables, err := job.GetVariablesAsMap()
	if err != nil {
		failJob(jobKey)
		return
	}

	if job.GetType() == "retry-offer-submit" {
		r := job.GetRetries() - 1
		if r < 0 {
			_, _ = zbClient.GetClient().NewUpdateJobRetriesCommand().JobKey(jobKey).Retries(r).Send(context.Background())
		}
		retriesError(CompleteJobDTO{WorkFlowInstanceKey:workflowInstanceKey, JobKey:jobKey, Variables:variables})
	}

	if job.GetType() == "retry-account-submit" {
		r := job.GetRetries() - 1
		if r < 0 {
			_, _ = zbClient.GetClient().NewUpdateJobRetriesCommand().JobKey(jobKey).Retries(r).Send(context.Background())
		}
		retriesError(CompleteJobDTO{WorkFlowInstanceKey:workflowInstanceKey, JobKey:jobKey, Variables:variables})
	}

	log.Printf("workflow instance key %d", workflowInstanceKey)
	log.Printf("job key %d", jobKey)

	publish(job.GetType(), jobKey, workflowInstanceKey, variables)
}

func CompleteJob(message Message) {
	request, err := zbClient.GetClient().NewCompleteJobCommand().JobKey(message.Headers.XJobKey).VariablesFromMap(message.Body)

	if err != nil {
		failJob(message.Headers.XJobKey)
		return
	}

	_, _ = request.Send(context.Background())

	log.Println(message.Headers)
}

func failJob(jobKey int64) {
	_, _ = zbClient.GetClient().NewFailJobCommand().JobKey(jobKey).Retries(0).Send(context.Background())
}

func publish(jobType string, jobKey int64, workflowInstanceKey int64, variables map[string]interface{})  {
	headers := Header{XJobKey:jobKey, XWorkflowInstanceKey:workflowInstanceKey}

	message := Message{Headers:headers, Body:variables}

	Publish(jobType, message)
}