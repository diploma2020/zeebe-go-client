package main

const (
	ZeebeBrokerAddr = "0.0.0.0:26500"
	HTTPPort        = "8000"
	RabbitUrl		= "amqp://guest:guest@localhost:5672/"
)

var JobTypes = map[string]string {
	"initialization"		: 	"initialization",
	"review-app"			: 	"review-app",
	"meeting"				: 	"meeting",
	"prepare-offer"			: 	"prepare-offer",
	"retry-offer-submit"	: 	"retry-offer-submit",
	"offer-response"		: 	"offer-response",
	"call-partner-offer"	: 	"call-partner-offer",
	"offer-call-response"	: 	"offer-call-response",
	"create-acc"			: 	"create-acc",
	"retry-account-submit"	: 	"retry-account-submit",
	"acc-response"			: 	"acc-response",
	"call-partner-acc"		: 	"call-partner-acc",
	"acc-call-response"		: 	"acc-call-response",
	"reject-app"			: 	"reject-app",
}

var JobTypesArr  = []string{
	"initialization",
	"review-app",
	"meeting",
	"prepare-offer",
	"retry-offer-submit",
	"offer-response",
	"call-partner-offer",
	"offer-call-response",
	"create-acc",
	"retry-account-submit",
	"acc-response",
	"call-partner-acc",
	"acc-call-response",
	"reject-app",
}