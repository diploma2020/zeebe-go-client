package main

import (
	"github.com/streadway/amqp"
	"log"
)

type Session struct {
	Connection *amqp.Connection
	Channel *amqp.Channel
}

func (s Session) Close() error {
	if s.Connection == nil {
		return nil
	}
	return s.Connection.Close()
}

var session Session

func InitRabbit()  {
	c := make(chan *amqp.Error)
	go func() {
		err := <-c
		log.Println("reconnect: " + err.Error())
		InitRabbit()
	}()

	conn, err := amqp.Dial(RabbitUrl)
	if err != nil {
		panic("cannot connect")
	}
	conn.NotifyClose(c)

	channel, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer channel.Close()

	err = channel.ExchangeDeclare(
		"zeebe.job", // name
		"topic",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	session = Session{Connection:conn, Channel:channel}

	Subscribe()
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}